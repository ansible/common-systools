---

systools_packages:
  - afuse                  # automounting file system implemented in user-space using FUSE
  - apt-file               # search for files within Debian packages
  - build-essential        # Informational list of build-essential packages
  - cifs-utils             # Common Internet File System utilities
  - colortest              # utilities to test color capabilities of terminal
  - cpu-checker            # for <kvm-ok>
  # https://unix.stackexchange.com/a/36193/411416
  - dateutils              # nifty commandline date and time utilities
  - diffutils              # file comparison utilities
  - dirmngr                # important for functionality of gnupg
  - fonts-liberation       # Liberation fonts
  - gnupg                  # GNU privacy guard - a free PGP replacement
  - gnupg-utils            # GNU privacy guard - utility programs
  - inkscape               # useful as converter in CLI mode
  # https://codeberg.org/smxi/inxi
  - inxi                   # full featured system information script
  - libimage-exiftool-perl # read and write meta information in multimedia files
  - librsvg2-bin           # command-line utility to convert SVG files
  - libtiff-tools          # contains the tiffinfo utility
  - liblz4-tool            # compress and decompress LZ4 archives
  - make                   # utility for directing compilation
  # Consider doing more to allow a git-like workflow in Mercurial repos
  # https://stackoverflow.com/questions/883452/git-interoperability-with-a-mercurial-repository
  - mercurial              # easy-to-use, scalable distributed version control system
  - moreutils              # additional Unix utilities (first installed to use "sponge", see bash-history in totalsync)
  - nbtscan                # scan networks searching for NetBIOS information
  - nfs-common             # for mounting NFS share on LAN
  - nmap                   # The Network Mapper
  - pdf2svg                # converts PDF documents to SVG files (one per page)
  - pdfgrep                # search in pdf files for strings matching a regular expression
  - pinentry-tty           # important for functionality of gnupg
  - pinentry-gtk2          # important for functionality of gnupg
  - pinentry-curses        # curses-based PIN or pass-phrase entry dialog for GnuPG
  - rdfind                 # find duplicate files utility, https://github.com/pauldreik/rdfind
  - rename                 # Perl extension for renaming multiple files
  # use `sudo updatedb` to force an update of plocate database
  # https://manpages.ubuntu.com/manpages/jammy/man8/updatedb.plocate.8.html
  - plocate                # much faster locate # locate not available otherwise on Jammy
  - speedtest-cli          # test internet bandwidth using speedtest.net

  ## system performance monitoring tools
  - bashtop                # Resource monitor that shows usage and stats # https://github.com/aristocratos/bashtop
  - htop                   # interactive processes viewer
  - iftop                  # displays bandwidth usage information on an network interface
  - iotop                  # simple top-like I/O monitor
  - sysstat                # system performance tools (the iostat tool, among others)
  - xosview                # X based system monitor (except it doesn't support RAID6)
